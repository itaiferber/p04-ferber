//
//  ASTContinuousButton.m
//  p04-ferber
//
//  Created by Itai Ferber on 2/21/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

#import "ASTContinuousButton.h"

#pragma mark Constants
static double const ASTContinuousButtonEdgeLayerStroke = 2.0;

#pragma mark - Private Interface
@interface ASTContinuousButton ()

#pragma mark - Private Properties
//! A timer used in scheduling when to fire off actions.
@property NSTimer *actionTimer;

/*!
 A frame representing the actual portion of the view that is interactive.

 The button view is supposed to be square, but if the button is created with a
 non-square frame, we occupy only the smaller of the two dimensions (and center
 in the other).
 */
@property CGRect interactiveFrame;

//! A drawn layer displaying the outer bounds of the button.
@property CAShapeLayer *edgeLayer;

@end

#pragma mark -
@implementation ASTContinuousButton

#pragma mark - Initialization
+ (instancetype)buttonWithFrame:(CGRect)frame
{
    return [[self alloc] initWithFrame:frame];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (!(self = [super initWithFrame:frame])) {
        return nil;
    }

    _frequency = 1.0;
    [self setUpLayers];
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)decoder
{
    if (!(self = [super initWithCoder:decoder])) {
        return nil;
    }

    _frequency = 1.0;
    [self setUpLayers];
    return self;
}

- (void)setUpLayers
{
    // The frame we were created with may or may not be square. However, the
    // joystick must be square. Get the smaller of the two dimensions for us to
    // use, and center in the other.
    CGRect frame = self.bounds;
    if (frame.size.width > frame.size.height) {
        CGFloat difference = frame.size.width - frame.size.height;
        frame.size.width = frame.size.height;
        frame.origin.x += difference / 2;
    } else {
        CGFloat difference = frame.size.height - frame.size.width;
        frame.size.height = frame.size.width;
        frame.origin.y += difference / 2;
    }

    self.interactiveFrame = frame;

    // The edge layer will provide an outer bounding circle for the inner stick
    // to move within.
    CGRect edgeBounds = CGRectInset((CGRect){.size = frame.size},
                                    ASTContinuousButtonEdgeLayerStroke,
                                    ASTContinuousButtonEdgeLayerStroke);
    CGPathRef edgePath = CGPathCreateWithEllipseInRect(edgeBounds, NULL);

    _edgeLayer = [CAShapeLayer layer];
    _edgeLayer.path = edgePath;
    _edgeLayer.fillColor = [UIColor clearColor].CGColor;
    _edgeLayer.strokeColor = [UIColor whiteColor].CGColor;
    _edgeLayer.lineWidth = ASTContinuousButtonEdgeLayerStroke;
    _edgeLayer.frame = frame;
    [self.layer addSublayer:_edgeLayer];
    CGPathRelease(edgePath);
}

#pragma mark - Accessors
//! Reject multi-touch interactions.
- (BOOL)isMultipleTouchEnabled
{
    return NO;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    CGRect frame = CGRectInset(self.interactiveFrame,
                               ASTContinuousButtonEdgeLayerStroke,
                               ASTContinuousButtonEdgeLayerStroke);
    CGPoint center = CGPointMake(CGRectGetMidX(frame),
                                 CGRectGetMidY(frame));

    CGFloat radius = frame.size.width - center.x;
    CGFloat distance = (CGFloat)hypot(point.x - center.x, point.y - center.y);
    return distance <= radius;
}

#pragma mark - Handling User Interaction
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    // Don't respond to multi-touch; -isMultipleTouchEnabled should already take
    // care of this, but just in case.
    if (touches.count > 1) {
        [[self nextResponder] touchesBegan:touches withEvent:event];
        return;
    }

    // Ensure the touch is within the interactive region.
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self];
    if (self.highlighted /* currently responding to another touch */ ||
        !CGRectContainsPoint(self.interactiveFrame, location)) {
        [[self nextResponder] touchesBegan:touches withEvent:event];
        return;
    }

    // Set up the touch state.
    self.highlighted = YES;
    self.edgeLayer.fillColor = [UIColor lightGrayColor].CGColor;
    [self sendActionsForControlEvents:UIControlEventTouchDown];

    // Set up a timer to continue "pressing" the button every time interval.
    self.actionTimer = [NSTimer scheduledTimerWithTimeInterval:self.frequency target:self selector:@selector(actionTimerDidFire:) userInfo:nil repeats:YES];
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.actionTimer invalidate];
    self.edgeLayer.fillColor = [UIColor clearColor].CGColor;
    [self sendActionsForControlEvents:UIControlEventTouchCancel];
    self.highlighted = NO;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.actionTimer invalidate];
    self.edgeLayer.fillColor = [UIColor clearColor].CGColor;
    CGPoint location = [[touches anyObject] locationInView:self];
    BOOL inside = [self pointInside:location withEvent:event];
    [self sendActionsForControlEvents:inside ? UIControlEventTouchUpInside
                                             : UIControlEventTouchUpOutside];
    self.highlighted = NO;
}

- (void)actionTimerDidFire:(NSTimer *)timer
{
    [self sendActionsForControlEvents:UIControlEventTouchDownRepeat];
}

@end
