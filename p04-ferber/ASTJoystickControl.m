//
//  ASTJoystickControl.m
//  p04-ferber
//
//  Created by Itai Ferber on 2/20/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

#import "ASTJoystickControl.h"

#pragma mark Constants
static double const ASTJoystickControlEdgeLayerStroke = 2.0;

#pragma mark - Private Interface
@interface ASTJoystickControl ()

#pragma mark - Private Properties
//! The user-facing vector property, but privately writable.
@property (readwrite) CGVector touchVector;

/*!
 A frame representing the actual portion of the view that is interactive.
 
 The joystick view is supposed to be square, but if the joystick is created with
 a non-square frame, we occupy only the smaller of the two dimensions (and
 center in the other).
 */
@property CGRect interactiveFrame;

//! A drawn layer displaying the outer bounds of the joystick.
@property CAShapeLayer *edgeLayer;

//! A drawn layer displaying the user-movable stick.
@property CAShapeLayer *stickLayer;

@end

#pragma mark -
@implementation ASTJoystickControl

#pragma mark - Initialization
+ (instancetype)joystickWithFrame:(CGRect)frame
{
    return [[self alloc] initWithFrame:frame];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (!(self = [super initWithFrame:frame])) {
        return nil;
    }

    [self setUpLayers];
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)decoder
{
    if (!(self = [super initWithCoder:decoder])) {
        return nil;
    }

    [self setUpLayers];
    return self;
}

- (void)setUpLayers
{
    // The frame we were created with may or may not be square. However, the
    // joystick must be square. Get the smaller of the two dimensions for us to
    // use, and center in the other.
    CGRect frame = self.bounds;
    if (frame.size.width > frame.size.height) {
        CGFloat difference = frame.size.width - frame.size.height;
        frame.size.width = frame.size.height;
        frame.origin.x += difference / 2;
    } else {
        CGFloat difference = frame.size.height - frame.size.width;
        frame.size.height = frame.size.width;
        frame.origin.y += difference / 2;
    }

    self.interactiveFrame = frame;

    // The edge layer will provide an outer bounding circle for the inner stick
    // to move within.
    CGRect edgeBounds = CGRectInset((CGRect){.size = frame.size},
                                    ASTJoystickControlEdgeLayerStroke,
                                    ASTJoystickControlEdgeLayerStroke);
    CGPathRef edgePath = CGPathCreateWithEllipseInRect(edgeBounds, NULL);

    _edgeLayer = [CAShapeLayer layer];
    _edgeLayer.path = edgePath;
    _edgeLayer.fillColor = [UIColor clearColor].CGColor;
    _edgeLayer.strokeColor = [UIColor whiteColor].CGColor;
    _edgeLayer.lineWidth = ASTJoystickControlEdgeLayerStroke;
    _edgeLayer.frame = frame;
    [self.layer addSublayer:_edgeLayer];
    CGPathRelease(edgePath);

    // The stick itself will be the moving part of the joystick, confined within
    // the outer circular edge.
    CGRect stickBounds = CGRectInset(edgeBounds,
                                     frame.size.width / 3,
                                     frame.size.height / 3);
    CGPathRef stickPath = CGPathCreateWithEllipseInRect(stickBounds, NULL);
    _stickLayer = [CAShapeLayer layer];
    _stickLayer.path = stickPath;
    _stickLayer.fillColor = [UIColor whiteColor].CGColor;
    _stickLayer.lineWidth = 0.0;
    _stickLayer.opacity = 1.0;
    _stickLayer.frame = frame;
    _stickLayer.anchorPoint = CGPointMake(0.5, 0.5);
    [self.layer addSublayer:_stickLayer];
    CGPathRelease(stickPath);
}

#pragma mark - Accessors
//! Reject multi-touch interactions.
- (BOOL)isMultipleTouchEnabled
{
    return NO;
}

#pragma mark - Setting the Vector
/*!
 Updates the location of the stick within the view and sets the touch vector.
 
 \param location The location within the view the view should respond to
 \returns Whether the given touch landed was within interactive bounds or not
 */
- (BOOL)updateWithTouchLocation:(CGPoint)location
{
    CGRect frame = CGRectInset(self.interactiveFrame,
                               ASTJoystickControlEdgeLayerStroke,
                               ASTJoystickControlEdgeLayerStroke);
    CGPoint center = CGPointMake(CGRectGetMidX(frame),
                                 CGRectGetMidY(frame));
    CGFloat radius = (CGFloat)(frame.size.width / 3.0);

    CGFloat distance = (CGFloat)hypot(location.x - center.x, location.y - center.y);
    BOOL inside = distance <= radius;
    if (!inside) {
        // We're outside of the circle; need to bound the point by the circle.
        CGFloat angle = (CGFloat)atan2(location.y - center.y, location.x - center.x);
        location = CGPointMake(center.x + radius * (CGFloat)cos(angle),
                               center.y + radius * (CGFloat)sin(angle));
    }

    // Use integral coordinates to update the stick and keep calculations clean.
    location = CGPointMake((CGFloat)round(location.x), (CGFloat)round(location.y));

    // Update the stick position without implicitly animating.
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    self.stickLayer.position = location;
    [CATransaction commit];

    self.touchVector = CGVectorMake((CGFloat)fmax(fmin((location.x - center.x) / radius, 1.0), -1.0),
                                    (CGFloat)fmax(fmin((location.y - center.y) / radius, 1.0), -1.0));
    return inside;
}

#pragma mark - Handling User Interaction
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    // Don't respond to multi-touch; -isMultipleTouchEnabled should already take
    // care of this, but just in case.
    if (touches.count > 1) {
        [[self nextResponder] touchesBegan:touches withEvent:event];
        return;
    }

    // Ensure the touch is within the interactive region.
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self];
    if (self.highlighted /* currently responding to another touch */ ||
        !CGRectContainsPoint(self.interactiveFrame, location)) {
        [[self nextResponder] touchesBegan:touches withEvent:event];
        return;
    }

    // Set up the touch state.
    self.highlighted = YES;
    self.edgeLayer.fillColor = [UIColor lightGrayColor].CGColor;
    [self updateWithTouchLocation:location];
    [self sendActionsForControlEvents:UIControlEventTouchDown];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    CGPoint location = [[touches anyObject] locationInView:self];
    BOOL inside = [self updateWithTouchLocation:location];
    [self sendActionsForControlEvents:inside ? UIControlEventTouchDragInside
                                             : UIControlEventTouchDragOutside];
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    self.edgeLayer.fillColor = [UIColor clearColor].CGColor;

    // Send the stick back to its default position.
    CGRect frame = self.interactiveFrame;
    CGPoint center = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
    [self updateWithTouchLocation:center];

    [self sendActionsForControlEvents:UIControlEventTouchCancel];
    self.highlighted = NO;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    self.edgeLayer.fillColor = [UIColor clearColor].CGColor;
    
    // Set the stick back to its default position.
    CGRect frame = self.interactiveFrame;
    CGPoint center = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
    BOOL inside = [self updateWithTouchLocation:center];

    [self sendActionsForControlEvents:inside ? UIControlEventTouchUpInside
                                             : UIControlEventTouchUpOutside];
    self.highlighted = NO;
}

@end
