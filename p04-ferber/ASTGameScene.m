//
//  ASTGameScene.m
//  p04-ferber
//
//  Created by Itai Ferber on 2/16/16.
//  Copyright (c) 2016 iferber1. All rights reserved.
//

#import "ASTGameScene.h"

#pragma mark Constants
/*!
 The categories of objects available in the game.
 */
NS_ENUM(uint32_t, ASTPhysicsCategory) {
    ASTPhysicsCategoryWalls    = 1 << 0,
    ASTPhysicsCategoryShip     = 1 << 1,
    ASTPhysicsCategoryBullet   = 1 << 2,
    ASTPhysicsCategoryAsteroid = 1 << 3
};

/*!
 The sizes of asteroids available in the game.
 */
typedef NS_ENUM(NSInteger, ASTAsteroidSizeClass) {
    ASTAsteroidSizeClassSmall,
    ASTAsteroidSizeClassMedium,
    ASTAsteroidSizeClassLarge
};

//! The initial number of asteroids to add to the game.
static NSInteger const ASTGameInitialAsteroidCount = 4;

//! The angle at which asteroids break off.
static CGFloat const ASTAsteroidBreakoffAngle = (CGFloat)M_PI_4;

//! THe file name containing the sound to play when the ship fires a laser.
static NSString * const ASTFireSoundFileName = @"pew.caf";

//! The file name containing the sound to play when an asteroid explodes.
static NSString * const ASTExplosionSoundFileName = @"explosion.caf";

#pragma mark - Helper Functions
/*!
 Returns a random point inside the given rectangle.
 
 \param rect The rectangle to pick a random point within.
 \discussion The rectangle must have a width and height which can be converted
             to 32-bit integers.
 \returns A CGPoint within the given rectangle.
 */
static inline CGPoint ASTRandomPointInRect(CGRect rect)
{
    return CGPointMake(rect.origin.x + arc4random_uniform((u_int32_t)rect.size.width),
                       rect.origin.y + arc4random_uniform((u_int32_t)rect.size.height));
}

/*!
 Creates a CGVector representing a velocity with the given magnitude and
 direction.
 
 \param magnitude The magnitude to give the vector.
 \param direction The direction to give the vector.
 \returns A new CGVector representing the requested velocity.
 */
static inline CGVector ASTMakeVelocity(CGFloat magnitude, CGFloat direction)
{
    return CGVectorMake(magnitude * (CGFloat)cos(direction),
                        magnitude * (CGFloat)sin(direction));
}

#pragma mark - Private Interface
@interface ASTGameScene ()

#pragma mark - Private Properties
//! The user's score.
@property (nonatomic) NSInteger score;

//! A label showing the user's score.
@property SKLabelNode *scoreLabel;

//! A node containing the game's tiled background.
@property SKNode *background;

//! The game's ship.
@property SKShapeNode *ship;

//! The asteroids currently in the game view.
@property NSMutableSet<SKShapeNode *> *asteroids;

@end

#pragma mark -
@implementation ASTGameScene

#pragma mark - Initialization
+ (void)load
{
    // Preload audio files.
    if (self == [ASTGameScene class]) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            (void)[SKAction playSoundFileNamed:ASTExplosionSoundFileName waitForCompletion:NO];
            (void)[SKAction playSoundFileNamed:ASTFireSoundFileName waitForCompletion:NO];
        });

    }
}

- (instancetype)initWithSize:(CGSize)size
{
    if (!(self = [super initWithSize:size])) {
        return nil;
    }

    // The ship is a custom-drawn object; this is the path that defines the look
    // of the ship.
    CGMutablePathRef shipPath = CGPathCreateMutable();
    CGPathMoveToPoint(shipPath, NULL, 0, 0);
    CGPathAddLineToPoint(shipPath, NULL, 20, 40);
    CGPathAddLineToPoint(shipPath, NULL, 40, 0);
    CGPathAddLineToPoint(shipPath, NULL, 25, 10);
    CGPathAddLineToPoint(shipPath, NULL, 15, 10);
    CGPathCloseSubpath(shipPath);

    // Create the ship from that path
    _ship = [SKShapeNode shapeNodeWithPath:shipPath centered:YES];
    _ship.strokeColor = [UIColor whiteColor];
    _ship.lineWidth = 3.0;
    CGPathRelease(shipPath);

    // Set up ship physics. The ship should collide with asteroids, and should
    // make contact with asteroids and the walls (so we know when the collision
    // happens, and when to wrap around the ship).
    _ship.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:_ship.path];
    _ship.physicsBody.categoryBitMask = ASTPhysicsCategoryShip;
    _ship.physicsBody.collisionBitMask = ASTPhysicsCategoryAsteroid;
    _ship.physicsBody.contactTestBitMask = ASTPhysicsCategoryWalls | ASTPhysicsCategoryAsteroid;
    _ship.physicsBody.dynamic = YES;
    _ship.physicsBody.affectedByGravity = NO;
    _ship.physicsBody.allowsRotation = NO;
    _ship.physicsBody.friction = 0.0;
    
    // Set up the environment; nothing should collide with the walls, and all
    // other objects will set up their own contact masks to check for wall
    // contact.
    self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
    self.physicsBody.categoryBitMask = ASTPhysicsCategoryWalls;
    self.physicsBody.collisionBitMask = 0;
    self.physicsWorld.contactDelegate = self;

    _asteroids = [NSMutableSet set];
    return self;
}

- (void)didMoveToView:(SKView *)view
{
    // We might be playing on differently-sized devices, so we want to create
    // a background image that tiles nicely to fit any sized device. However,
    // SpriteKit doesn't offer native facilities to tile images; pattern colors
    // don't work, so we have to create a background view ourselves.
    // We can create a node containing multiple sprite nodes, manually tiling
    // the image, then using that node as our background.
    self.background = [self createTiledBackgroundOfSize:view.bounds.size
                                              fromImage:[UIImage imageNamed:@"Background"]];
    [self addChild:self.background];

    self.scoreLabel = [SKLabelNode labelNodeWithText:@"Score: 0"];
    self.scoreLabel.fontSize = 24;

    CGSize labelSize = self.scoreLabel.frame.size;
    self.scoreLabel.position = CGPointMake(labelSize.width,
                                           CGRectGetMaxY(self.frame) - labelSize.height * 5 / 2);
    [self addChild:self.scoreLabel];

    [self resetGame];
}

/*!
 Returns a new SKNode, containing sprite nodes as children which combine to form
 a tiled background of at least the given size.
 
 \param size The minimum size to create the background for. The resulting
             texture may be larger, and may need to be cropped.
 \param image The image to tile.
 \return A new SKNode containing SKSpriteNodes as children.
 */
- (SKNode *)createTiledBackgroundOfSize:(CGSize)size fromImage:(nonnull UIImage *)image
{
    SKNode *container = [SKNode node];
    CGSize imageSize = image.size;

    for (NSInteger row = 0; row <= size.height / imageSize.height + 1; row += 1) {
        for (NSInteger column = 0; column <= size.width / imageSize.width + 1; column += 1) {
            SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImage:image]];
            sprite.position = CGPointMake(column * imageSize.width, row * imageSize.height);
            [container addChild:sprite];
        }
    }

    return container;
}

#pragma mark - Accessors
- (void)setScore:(NSInteger)score
{
    _score = score;
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %ld", (long)score];
}

#pragma mark - Creating Asteroids
/*!
 Returns a mutable path representing one of the three possible asteroid types.
 
 \param type The type of asteroid to draw.
 \returns An editable CGMutablePathRef containing the drawn path.
 */
- (CGMutablePathRef)newAsteroidPathOfType:(NSInteger)type
{
    CGMutablePathRef asteroidPath = CGPathCreateMutable();
    if (type == 0) {
        CGPathMoveToPoint(asteroidPath, NULL, 15, 3);
        CGPathAddLineToPoint(asteroidPath, NULL, 28, 7);
        CGPathAddLineToPoint(asteroidPath, NULL, 30, 18);
        CGPathAddLineToPoint(asteroidPath, NULL, 22, 24);
        CGPathAddLineToPoint(asteroidPath, NULL, 7, 27);
        CGPathAddLineToPoint(asteroidPath, NULL, 0, 19);
        CGPathAddLineToPoint(asteroidPath, NULL, 3, 12);
        CGPathAddLineToPoint(asteroidPath, NULL, 3, 4);
    } else if (type == 1) {
        CGPathMoveToPoint(asteroidPath, NULL, 21, 33);
        CGPathAddLineToPoint(asteroidPath, NULL, 17, 23);
        CGPathAddLineToPoint(asteroidPath, NULL, 21, 15);
        CGPathAddLineToPoint(asteroidPath, NULL, 18, 6);
        CGPathAddLineToPoint(asteroidPath, NULL, 32, 3);
        CGPathAddLineToPoint(asteroidPath, NULL, 48, 14);
        CGPathAddLineToPoint(asteroidPath, NULL, 40, 27);
    } else {
        CGPathMoveToPoint(asteroidPath, NULL, 19, 2);
        CGPathAddLineToPoint(asteroidPath, NULL, 28, 6);
        CGPathAddLineToPoint(asteroidPath, NULL, 30, 13);
        CGPathAddLineToPoint(asteroidPath, NULL, 39, 13);
        CGPathAddLineToPoint(asteroidPath, NULL, 34, 34);
        CGPathAddLineToPoint(asteroidPath, NULL, 15, 32);
        CGPathAddLineToPoint(asteroidPath, NULL, 7, 21);
        CGPathAddLineToPoint(asteroidPath, NULL, 10, 8);
        CGPathAddLineToPoint(asteroidPath, NULL, 19, 10);
    }

    CGPathCloseSubpath(asteroidPath);
    return asteroidPath;
}

/*!
 Creates a new asteroid of the given type and size.
 
 \param type The type of asteroid to draw.
 \param size The size class of the asteroid to create.
 \returns A newly-created asteroid object, ready to be positioned and added to
          the scene.
 */
- (SKShapeNode *)createAsteroidOfType:(NSInteger)type sizeClass:(ASTAsteroidSizeClass)size
{
    CGFloat scale = 1.0;
    if (size == ASTAsteroidSizeClassLarge) {
        scale = 2.0;
    } else if (size == ASTAsteroidSizeClassSmall) {
        scale = (CGFloat)(2.0 / 3.0);
    }

    // We can create a path for the given type of asteroid, then scale it up or
    // down with an affine transform before creating an asteroid out of it.
    CGMutablePathRef path = [self newAsteroidPathOfType:type];
    CGAffineTransform transform = CGAffineTransformMakeScale(scale, scale);
    CGPathRef asteroidPath = CGPathCreateCopyByTransformingPath(path, &transform);
    CGPathRelease(path);

    // Create the asteroid itself from that shape.
    SKShapeNode *asteroid = [SKShapeNode shapeNodeWithPath:asteroidPath centered:YES];
    asteroid.strokeColor = [UIColor whiteColor];
    asteroid.lineWidth = 2.0;
    asteroid.userData = [@{@"type": @(type), @"size": @(size)} mutableCopy];
    CGPathRelease(asteroidPath);

    // Set up asteroid collision and contact physics.
    asteroid.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:asteroid.path];
    asteroid.physicsBody.categoryBitMask = ASTPhysicsCategoryAsteroid;
    asteroid.physicsBody.collisionBitMask = ASTPhysicsCategoryAsteroid;
    asteroid.physicsBody.contactTestBitMask = ASTPhysicsCategoryWalls;
    asteroid.physicsBody.dynamic = YES;
    asteroid.physicsBody.affectedByGravity = NO;
    asteroid.physicsBody.allowsRotation = YES;
    asteroid.physicsBody.friction = 0.0;
    asteroid.physicsBody.restitution = 1.0;
    asteroid.physicsBody.linearDamping = 0.0;
    asteroid.physicsBody.angularDamping = 0.0;
    return asteroid;
}

#pragma mark - Game Setup
- (void)resetGame
{
    self.paused = YES;
    
    // Need to remove the asteroids currently in the field.
    for (SKShapeNode *asteroid in self.asteroids) {
        [asteroid removeFromParent];
    }

    [self.asteroids removeAllObjects];

    // Center the ship to begin with.
    [self.ship removeFromParent];
    self.ship.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    self.ship.zRotation = 0;
    [self addChild:self.ship];

    // Reset the score.
    self.score = 0;
}

- (void)startGame
{
    // Add asteroids.
    for (NSInteger i = 0; i < ASTGameInitialAsteroidCount; i += 1) {
        // Create one of three different shapes of asteroids.
        NSInteger asteroidType = (NSInteger)arc4random_uniform(3);

        SKShapeNode *asteroid = [self createAsteroidOfType:asteroidType sizeClass:ASTAsteroidSizeClassLarge];
        [self.asteroids addObject:asteroid];

        // Now we have to find a place to put the asteroid. We want to make sure the
        // asteroid is placed somewhere that's not too close to the ship, and within
        // the bounds of the screen.
        // Inset the "available frame" to make sure the asteroid will fit entirely
        // within the screen.
        CGSize asteroidSize = CGPathGetBoundingBox(asteroid.path).size;
        CGRect availableFrame = CGRectInset(self.frame, asteroidSize.width, asteroidSize.height);

        // Want to make sure the asteroid isn't too close to the ship itself.
        do {
            asteroid.position = ASTRandomPointInRect(availableFrame);

            // The asteroid would overlap with another asteroid.
            if ([self.physicsWorld bodyInRect:asteroid.frame] != nil) {
                continue;
            }
        } while (fabs(self.ship.position.x - asteroid.position.x) < 100 ||
                 fabs(self.ship.position.y - asteroid.position.y) < 100);

        [self addChild:asteroid];

        // Now that the asteroid is part of the scene, we have to pick a random
        // rotation and velocity for it.
        CGFloat angle = (CGFloat)(arc4random_uniform(360) / 180 * M_PI);
        asteroid.zRotation = angle - (CGFloat)M_PI_2;
        asteroid.physicsBody.velocity = ASTMakeVelocity(100, angle);
    }

    self.physicsWorld.contactDelegate = self;
    self.paused = NO;
}

#pragma mark - User Interaction
- (void)joystickDidMove:(ASTJoystickControl *)joystick
{
    // Apply the appropriate rotation and velocity to the ship.
    CGVector velocty = joystick.touchVector;
    CGFloat angle = (CGFloat)atan2(-velocty.dy, velocty.dx);
    self.ship.zRotation = angle - (CGFloat)M_PI_2;

    velocty.dx *= 200;
    velocty.dy *= -200;
    self.ship.physicsBody.velocity = velocty;
}

- (void)joystickWasReleased:(ASTJoystickControl *)joystick
{
    // Stop the ship.
    self.ship.physicsBody.velocity = CGVectorMake(0, 0);
}

- (void)fire:(ASTContinuousButton *)button
{
    // Calculate the position that the bullet would appear at. Start as if the
    // bullet were to appear right above the ship when it is pointing straight
    // up, and rotate around the ship's center.
    CGFloat angle = self.ship.zRotation + (CGFloat)M_PI_2;
    CGFloat radius = self.ship.frame.size.height / 2;
    CGPoint bulletPosition = CGPointMake(self.ship.position.x + radius * (CGFloat)cos(angle),
                                         self.ship.position.y + radius * (CGFloat)sin(angle));

    if (!CGRectContainsPoint(self.frame, bulletPosition)) {
        // We're beginning the firing action while the end of the ship is
        // off-screen. Modify to wrap around.
        if (bulletPosition.x < self.frame.origin.x) {
            bulletPosition.x = self.frame.size.width;
        } else if (bulletPosition.x > self.frame.size.width) {
            bulletPosition.x = self.frame.origin.x;
        }

        if (bulletPosition.y < self.frame.origin.y) {
            bulletPosition.y = self.frame.size.height;
        } else if (bulletPosition.y > self.frame.size.height) {
            bulletPosition.y = self.frame.origin.y;
        }
    }

    // The bullet will be a rectangle; we need to explicitly create a path here
    // so we can ask for it to be centered in the bullet node -- otherwise its
    // appearance won't be visually centered.
    CGRect bulletFrame = CGRectMake(0, 0, 4.0, 10.0);
    CGPathRef bulletPath = CGPathCreateWithRect(bulletFrame, NULL);
    SKShapeNode *bullet = [SKShapeNode shapeNodeWithPath:bulletPath centered:YES];
    bullet.strokeColor = [UIColor whiteColor];
    bullet.fillColor = [UIColor whiteColor];
    bullet.position = bulletPosition;
    bullet.zRotation = self.ship.zRotation;
    CGPathRelease(bulletPath);

    // Set up physics as we would for the ship. Bullets collide with asteroids,
    // and we care to know when they make contact with those asteroids (and the
    // walls) so that we can perform the correct behavior.
    bullet.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:bullet.path];
    bullet.physicsBody.categoryBitMask = ASTPhysicsCategoryBullet;
    bullet.physicsBody.collisionBitMask = ASTPhysicsCategoryAsteroid;
    bullet.physicsBody.contactTestBitMask = ASTPhysicsCategoryWalls | ASTPhysicsCategoryAsteroid;
    bullet.physicsBody.dynamic = YES;
    bullet.physicsBody.affectedByGravity = NO;
    bullet.physicsBody.allowsRotation = NO;
    bullet.physicsBody.friction = 0.0;
    [self addChild:bullet];

    // Now that the bullet is in the scene, we can apply a velocity to it.
    bullet.physicsBody.velocity = ASTMakeVelocity(300, angle);

    // Bullets last on screen for 1.5 seconds before fading out. Once they fade
    // out, they should be removed from the view.
    SKAction *fadeOut = [SKAction fadeOutWithDuration:1.5];
    fadeOut.timingMode = SKActionTimingEaseIn;
    [bullet runAction:fadeOut completion:^{
        [bullet removeFromParent];
    }];

    // Pew pew!
    [self runAction:[SKAction playSoundFileNamed:ASTFireSoundFileName waitForCompletion:NO]];
}

#pragma mark - Contacts and Collisions
- (void)update:(NSTimeInterval)currentTime
{
    for (SKNode *node in self.children) {
        if (node.physicsBody.categoryBitMask > ASTPhysicsCategoryWalls) {
            CGRect intersection = CGRectIntersection(self.frame, node.frame);

            // The object just came into the view; no need to do anything.
            if (!CGRectIsNull(intersection)) {
                continue;
            }

            CGPoint position = node.position;

            // This velocity will be lost when the object is removed from the world;
            // hold on to it to apply it later.
            CGVector velocity = node.physicsBody.velocity;

            // Remove from the parent so we can move the object around as needed.
            [node removeFromParent];

            // Wrap the object around in the x direction if necessary.
            if (position.x < self.frame.origin.x) {
                position.x = self.frame.size.width;
            } else if (position.x > self.frame.size.width) {
                position.x = self.frame.origin.x;
            }

            // Wrap the object around in the y direction if necessary.
            if (node.position.y < self.frame.origin.y) {
                position.y = self.frame.size.height;
            } else if (position.y > self.frame.size.height) {
                position.y = self.frame.origin.y;
            }

            // Once the object has been moved, it's ready to add back to the scene.
            node.position = position;
            [self addChild:node];

            // Restore the velocity it had before. This only works after the object
            // has been added back to the scene.
            node.physicsBody.velocity = velocity;
        }
    }
}

- (void)didBeginContact:(SKPhysicsContact *)contact
{
    BOOL normalized = NO;
    SKPhysicsBody *bodyA, *bodyB;
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
        bodyA = contact.bodyA;
        bodyB = contact.bodyB;
    } else {
        bodyA = contact.bodyB;
        bodyB = contact.bodyA;
        normalized = YES;
    }

    if (bodyA.categoryBitMask == ASTPhysicsCategoryWalls) {
        // Something collided with the walls. We don't care about that here.
        return;
    } else if (bodyA.categoryBitMask == ASTPhysicsCategoryShip) {
        // The ship collided with an asteroid; the game is starting to end, so
        // ignore additional contacts.
        self.physicsWorld.contactDelegate = nil;

        // Make the ship explode.
        NSString *sparksResourcePath = [[NSBundle mainBundle] pathForResource:@"Sparks" ofType:@"sks"];
        NSAssert(sparksResourcePath != nil, @"Unable to load sparks resource.");
        SKEmitterNode *effect = [NSKeyedUnarchiver unarchiveObjectWithFile:sparksResourcePath];
        effect.position = self.ship.position;
        [self addChild:effect];

        [self runAction:[SKAction playSoundFileNamed:ASTExplosionSoundFileName waitForCompletion:NO]];
        [self.ship removeFromParent];

        // Break up the asteroid it collided with.
        [self breakUpAsteroid:(SKShapeNode *)bodyB.node withContact:contact normalized:normalized];

        // End the game.
        [self.gameDelegate game:self didEndWithScore:self.score];
    } else if (bodyA.categoryBitMask == ASTPhysicsCategoryBullet) {
        [bodyA.node removeFromParent];

        SKShapeNode *asteroid = (SKShapeNode *)bodyB.node;
        NSInteger value = ((ASTAsteroidSizeClassLarge + 1) - [asteroid.userData[@"size"] integerValue]) * 10;
        self.score += value;

        [self runAction:[SKAction playSoundFileNamed:ASTExplosionSoundFileName waitForCompletion:NO]];
        [self breakUpAsteroid:asteroid withContact:contact normalized:normalized];

        // The user got rid of all the asteroids. Give a bonus and add more!
        if (self.asteroids.count == 0) {
            self.score += 2000;
            [self startGame];
        }
    } else {
        NSLog(@"Something made contact that shouldn't have!");
    }
}

/*!
 Breaks up the given asteroid with information about the contact breaking it up.
 
 \param asteroid The asteroid to break up.
 \param contact The contact object representing the collision with the asteroid.
 */
- (void)breakUpAsteroid:(SKShapeNode *)asteroid withContact:(SKPhysicsContact *)contact normalized:(BOOL)normalized
{
    if (!asteroid) {
        return;
    }

    // Add an explosion effect.
    NSString *explosionEffectPath = [[NSBundle mainBundle] pathForResource:@"Explosion" ofType:@"sks"];
    NSAssert(explosionEffectPath != nil, @"Unable to load explosion effect.");

    SKEmitterNode *effect = [NSKeyedUnarchiver unarchiveObjectWithFile:explosionEffectPath];
    effect.position = asteroid.position;
    [self addChild:effect];

    // The asteroid blows up and disappears.
    [self.asteroids removeObject:asteroid];
    [asteroid removeFromParent];

    // If it needs to break up, break it up.
    ASTAsteroidSizeClass size = [asteroid.userData[@"size"] integerValue];
    if (size > ASTAsteroidSizeClassSmall) {
        // Need to create smaller asteroids which have broken off.
        NSInteger type = [asteroid.userData[@"type"] integerValue];
        SKShapeNode *asteroid1 = [self createAsteroidOfType:type sizeClass:size - 1],
                    *asteroid2 = [self createAsteroidOfType:type sizeClass:size - 1];

        CGVector contactNormal = contact.contactNormal;
        if (!normalized) {
            contactNormal.dx = -contactNormal.dx;
            contactNormal.dy = -contactNormal.dy;
        }

        CGFloat contactAngle = (CGFloat)(atan2(contactNormal.dy, contactNormal.dx) + M_PI);
        CGFloat distance = (CGFloat)hypot(contact.contactPoint.x - asteroid.position.x,
                                          contact.contactPoint.y - asteroid.position.y);

        void (^launch)(SKShapeNode*, CGFloat) = ^(SKShapeNode *body, CGFloat angle) {
            body.position = CGPointMake(contact.contactPoint.x + distance * (CGFloat)cos(angle),
                                        contact.contactPoint.y + distance * (CGFloat)sin(angle));

            [self addChild:body];
            [self.asteroids addObject:body];

            body.zRotation = angle - (CGFloat)M_PI_2;
            body.physicsBody.velocity = ASTMakeVelocity(100, angle);
        };

        // Make the asteroids fly off at opposing angles.
        launch(asteroid1, contactAngle - ASTAsteroidBreakoffAngle);
        launch(asteroid2, contactAngle + ASTAsteroidBreakoffAngle);
    }
}

@end
