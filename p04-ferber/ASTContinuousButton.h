//
//  ASTContinuousButton.h
//  p04-ferber
//
//  Created by Itai Ferber on 2/21/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

@import UIKit;

/*!
 A button which fires its action continuously.
 */
@interface ASTContinuousButton : UIControl

#pragma mark - Properties
//! How often the button should fire off its action when pressed.
@property NSTimeInterval frequency;

#pragma mark - Initialization
/*!
 Creates a new joystick occupying the given frame.

 \param frame The origin and size to assign to the joystick
 \return A new joystick control
 */
+ (instancetype)buttonWithFrame:(CGRect)frame;

/*!
 Initializes the receiver with the given frame.

 \param frame The origin and size to assign to the receiver
 \return The initialized receiver
 */
- (instancetype)initWithFrame:(CGRect)frame;

@end
