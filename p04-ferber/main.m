//
//  main.m
//  p04-ferber
//
//  Created by Itai Ferber on 2/16/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

@import UIKit;
#import "ASTAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ASTAppDelegate class]));
    }
}
