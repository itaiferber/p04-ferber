//
//  ASTGameDelegate.h
//  p04-ferber
//
//  Created by Itai Ferber on 2/22/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

@class ASTGameScene;

@protocol ASTGameDelegate <NSObject>
@required

/*!
 Called when the given game has ended.
 
 \param game The game scene that has just ended.
 \param score The user's final score.
 */
- (void)game:(ASTGameScene *)game didEndWithScore:(NSInteger)score;

@end
