//
//  ASTGameViewController.m
//  p04-ferber
//
//  Created by Itai Ferber on 2/16/16.
//  Copyright (c) 2016 iferber1. All rights reserved.
//

@import SpriteKit;
#import "ASTGameViewController.h"
#import "ASTGameScene.h"

@implementation ASTGameViewController

#pragma mark - View Life Cycle
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    // Don't allow the user to start playing yet.
    self.joystick.userInteractionEnabled = NO;
    self.fireButton.userInteractionEnabled = NO;

    // SpriteKit should NOT ignore sibling order for this game; it prevents some
    // sprites from appearing properly, and the game is not so intensive that we
    // require the optimization.
    self.gameView.ignoresSiblingOrder = NO;

    // Load up the scene and show it.
    ASTGameScene *scene = [ASTGameScene sceneWithSize:self.view.bounds.size];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    [self.gameView presentScene:scene];
    ((ASTGameScene *)self.gameView.scene).gameDelegate = self;

    // The joystick sends touch events for all interaction. We care separately
    // about when the joystick is initially down or moving, and when it's
    // released -- the joystick control sends a final message when a touch is
    // canceled or released, but at that point, its touch vector is (0, 0). For
    // that event specifically, we want to set the ship's velocity, but not
    // change its rotation.
    [self.joystick addTarget:scene action:@selector(joystickDidMove:) forControlEvents:UIControlEventTouchDown | UIControlEventTouchDragInside | UIControlEventTouchDragOutside];
    [self.joystick addTarget:scene action:@selector(joystickWasReleased:) forControlEvents:UIControlEventTouchCancel | UIControlEventTouchUpInside | UIControlEventTouchUpOutside];

    // Holding down the fire button should shoot every half a second.
    self.fireButton.frequency = 0.5;

    // The fire button sends a UIControlEventTouchDown down when the button is
    // initially pressed, and then a UIControlEventTouchDownRepeat every half
    // a second. We care about those, but not if the button was released.
    [self.fireButton addTarget:scene action:@selector(fire:) forControlEvents:UIControlEventTouchDown | UIControlEventTouchDownRepeat];
}

#pragma mark - Game State
- (void)game:(ASTGameScene *)game didEndWithScore:(NSInteger)score
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Game Over"
                                                                             message:[NSString stringWithFormat:@"You asphyxiate in the endless, incomprehensible vastness of the cosmos. At least your score was %lld!", (long long)score]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Start Over" style:UIAlertActionStyleDefault handler:^(UIAlertAction *_) {
        self.joystick.userInteractionEnabled = NO;
        self.fireButton.userInteractionEnabled = NO;
        [(ASTGameScene *)self.gameView.scene resetGame];
        self.introView.alpha = 1.0;
    }]];

    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Actions
- (IBAction)startGame:(UIButton *)button
{
    self.introView.alpha = 0.0;
    self.joystick.userInteractionEnabled = YES;
    self.fireButton.userInteractionEnabled = YES;
    [(ASTGameScene *)self.gameView.scene startGame];
}

@end
