//
//  ASTGameViewController.h
//  p04-ferber
//
//  Created by Itai Ferber on 2/16/16.
//  Copyright (c) 2016 iferber1. All rights reserved.
//

@import UIKit;
#import "ASTJoystickControl.h"
#import "ASTContinuousButton.h"
#import "ASTGameDelegate.h"

@interface ASTGameViewController : UIViewController <ASTGameDelegate>

#pragma mark - Properties
//! The SpriteKit view responsible for displaying the game.
@property IBOutlet SKView *gameView;

//! The joystick responsible for moving the ship around the game.
@property IBOutlet ASTJoystickControl *joystick;

//! The button responsible for making the ship shoot bullets.
@property IBOutlet ASTContinuousButton *fireButton;

//! A view that shows instructions on the screen for the game.
@property IBOutlet UIView *introView;

#pragma mark - Actions
/*!
 Removes the intro screen and starts off the game.
 */
- (IBAction)startGame:(UIButton *)button;

@end
