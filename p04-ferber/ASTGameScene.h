//
//  ASTGameScene.h
//  p04-ferber
//
//  Created by Itai Ferber on 2/16/16.
//  Copyright (c) 2016 iferber1. All rights reserved.
//

@import SpriteKit;
#import "ASTJoystickControl.h"
#import "ASTContinuousButton.h"
#import "ASTGameDelegate.h"

@interface ASTGameScene : SKScene <SKPhysicsContactDelegate>

#pragma mark - Properties
//! An object that gets informed when the game ends.
@property (weak) id<ASTGameDelegate> gameDelegate;

#pragma mark - Game Setup
/*!
 Resets the receiver to starting position. The game is paused until otherwise
 unpaused.
 */
- (void)resetGame;

/*!
 Starts the game by adding asteroids to the field and unpausing.
 */
- (void)startGame;

#pragma mark - User Interaction
/*!
 A method to call when the given joystick has moved.
 
 \param joystick The joystick whose action to register.
 */
- (void)joystickDidMove:(ASTJoystickControl *)joystick;

/*!
 A method to call when the given joystick has been released.
 
 \param joystick The joystick whose action to register.
 */
- (void)joystickWasReleased:(ASTJoystickControl *)joystick;

/*!
 A method to call when the given button has fired, and the ship should shoot a
 bullet.
 
 \param button The button that was pressed.
 */
- (void)fire:(ASTContinuousButton *)button;

@end
