//
//  ASTJoystickControl.h
//  p04-ferber
//
//  Created by Itai Ferber on 2/20/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

@import UIKit;

/*!
 A UI control representing an interactive joy stick.
 */
@interface ASTJoystickControl : UIControl

#pragma mark - Properties
/*!
 A normalized (0.0–1.0) vector representing the direction and magnitude of the
 user's touch.
 
 The vector (0.5, 0) represents the user dragging at a 0° angle, halfway to the
 edge of the view. The vector (0, -1.0) represents the user dragging at a 270°
 angle directly down, all the way to the edge of the view.
 */
@property (readonly) CGVector touchVector;

#pragma mark - Initialization
/*!
 Creates a new joystick occupying the given frame.
 
 \param frame The origin and size to assign to the joystick
 \return A new joystick control
 */
+ (instancetype)joystickWithFrame:(CGRect)frame;

/*!
 Initializes the receiver with the given frame.
 
 \param frame The origin and size to assign to the receiver
 \return The initialized receiver
 */
- (instancetype)initWithFrame:(CGRect)frame;

@end
