//
//  ASTAppDelegate.h
//  p04-ferber
//
//  Created by Itai Ferber on 2/16/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

@import UIKit;

@interface ASTAppDelegate : UIResponder <UIApplicationDelegate>

#pragma mark - Properties
//! The application's main window.
@property (nonatomic) UIWindow *window;

@end
