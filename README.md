# Asteroids
This is a simple implementation of Asteroids, as specified in the CS 441 project requirements, by Stanley Chan and Itai Ferber. It handles movement of a ship, physically realistic asteroids and movement, and movement and shooting via a joystick and button. The game plays forever, awarding the user 2000 points when the screen is cleared of asteroids and spawning new ones. All you can do is hope to stave off death for as long as you can.

## Minor Bugs
There is a bug where asteroids will occasionally spawn on top of one another, moving together as one object. This is fixed by shooting one of the asteroids. Additionally, there was a latent bug until recently where shooting an asteroid near or off the edge of the screen (causing it to split off new asteroids off-screen) caused new asteroids to be created and lost forever; this should have been fixed in the last commit.
